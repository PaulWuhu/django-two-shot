from django.urls import path
from accounts.views import Log_in, log_out, signup

urlpatterns = [
    path("login/", Log_in, name="login"),
    path("logout/", log_out, name="logout"),
    path("signup/", signup, name="signup"),
]
