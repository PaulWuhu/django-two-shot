from receipts.views import (
    receipte_list_view,
    create_new_receipt,
    account_list,
    category_list,
    create_category,
    create_account,
)
from django.urls import path

urlpatterns = [
    path("", receipte_list_view, name="home"),
    path("create/", create_new_receipt, name="create_receipt"),
    path("accounts/", account_list, name="account_list"),
    path("categories/", category_list, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
