from django.contrib import admin
from receipts.models import Account, ExpenseCategory, Receipt


# Register your models here.
@admin.register(Account)
class receiptsAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "owner",
        "number",
    )


@admin.register(ExpenseCategory)
class receiptsAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "owner",
    )


@admin.register(Receipt)
class receiptsAdmin(admin.ModelAdmin):
    list_display = ("vendor", "total", "tax", "category", "account")
