from django.shortcuts import redirect, render
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import (
    CreateReceiptForm,
    CreateCategoryForm,
    CreateAccountForm,
)


@login_required
def receipte_list_view(request):
    receipte_list = Receipt.objects.filter(purchaser=request.user)
    context = {"receipte": receipte_list}
    return render(request, "receipt/home.html", context)


@login_required
def create_new_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = CreateReceiptForm()
    context = {"form": form}
    return render(request, "receipt/createnewreceipt.html", context)


@login_required
def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category_list": category_list}
    return render(
        request,
        "receipt/receipts_categories.html",
        context,
    )


@login_required
def account_list(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {"account_list": account_list}
    return render(
        request,
        "receipt/receipts_accounts.html",
        context,
    )


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = CreateCategoryForm()
    context = {"form": form}
    return render(request, "receipt/createcategories.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = CreateAccountForm()
    context = {"form": form}
    return render(request, "receipt/createaccount.html", context)
